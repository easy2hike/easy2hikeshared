package com.easy2hike.easyandroid.components;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.esri.arcgisruntime.ArcGISRuntimeException;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.geometry.Geometry;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.Polygon;
import com.esri.arcgisruntime.geometry.PolygonBuilder;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.loadable.LoadStatusChangedEvent;
import com.esri.arcgisruntime.loadable.LoadStatusChangedListener;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.MobileMapPackage;
import com.esri.arcgisruntime.tasks.offlinemap.GenerateOfflineMapJob;
import com.esri.arcgisruntime.tasks.offlinemap.GenerateOfflineMapParameters;
import com.esri.arcgisruntime.tasks.offlinemap.GenerateOfflineMapResult;
import com.esri.arcgisruntime.tasks.offlinemap.OfflineMapTask;

import java.io.File;
import java.util.Map;


/*
 * Code example of downloading an ArcGIS map for offline usage.
 *
 * Created by uri on 08/05/2018.
 */
public class DownloadOfflineMap {

    private Context context;
    private static final String TAG = "DownloadOfflineMap";

    public DownloadOfflineMap(Context context) {
        this.context = context;
    }

    private void downloadRouteOfflineEsri() {
        File offlineMapDirRoot = context.getDir(EsriMapFragment.offlineMapDirName, Context.MODE_PRIVATE);
        final File offlineMapDir = new File(offlineMapDirRoot, "test route Id");
        Log.i(TAG, "Getting webmap");
        try {
            String baseMapUrl = "https://easy2hike.maps.arcgis.com/home/webmap/viewer.html?webmap=3211e0a4026643ebb64a888cdcb18106";
            final ArcGISMap map = new ArcGISMap(baseMapUrl);
            map.addDoneLoadingListener(new Runnable() {
                public void run() {
                    if (map.getLoadStatus() == LoadStatus.LOADED) {
                        Log.i(TAG, "webmap loading complete");

                        // create an offline map task
                        final OfflineMapTask offlineMapTask = new OfflineMapTask(map);

                        // Create default parameters
                        final ListenableFuture<GenerateOfflineMapParameters> parametersFuture =
                                offlineMapTask.createDefaultGenerateOfflineMapParametersAsync(getOfflineBoxGeometry(map.getSpatialReference()));
                        parametersFuture.addDoneListener(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Log.i(TAG, "Starting offline download from ArcGIS");
                                    GenerateOfflineMapParameters generateOfflineMapParameters = parametersFuture.get();

                                    generateOfflineMapParameters.setIncludeBasemap(true);
                                    generateOfflineMapParameters.setMinScale(288895.288400);
                                    generateOfflineMapParameters.setMaxScale(4513.988880);
                                    // Create and start a job to generate the offline map
                                    final GenerateOfflineMapJob generateOfflineJob =
                                            offlineMapTask.generateOfflineMap(generateOfflineMapParameters, offlineMapDir.getAbsolutePath());
                                    Log.i(TAG, "Generating offline download from ArcGIS");
                                    generateOfflineJob.start();
                                    // Add progress changed listener to show the download percentage:
                                    generateOfflineJob.addProgressChangedListener(new Runnable() {
                                        @Override
                                        public void run() {
                                            setPercentage(generateOfflineJob.getProgress());
                                        }
                                    });
                                    generateOfflineJob.addJobDoneListener(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Generate the offline map and download it
                                            Log.i(TAG, "Running offline download from ArcGIS done");
                                            GenerateOfflineMapResult result = generateOfflineJob.getResult();

                                            if (result == null) {
                                                Log.e(TAG, "status is: " + generateOfflineJob.getStatus().name());
                                                Log.e(TAG, "error is: " + generateOfflineJob.getError().getMessage());
                                                Log.e(TAG, "error add is: " + generateOfflineJob.getError().getAdditionalMessage());
                                                // End download on error
                                                Toast.makeText(context, "Offline failed with null result", Toast.LENGTH_LONG).show();
                                                return;
                                            }

                                            if (!result.hasErrors()) {
                                                MobileMapPackage mobileMapPackage = result.getMobileMapPackage();
                                                // Job is finished and all content was generated
                                                Log.i(TAG, "Map " + mobileMapPackage.getItem().getTitle() +
                                                        " saved to " + mobileMapPackage.getPath());
                                                Toast.makeText(context, "Offline finished successfully - hip hip!", Toast.LENGTH_LONG).show();
                                            } else {
                                                // Job is finished but some of the layers/tables had errors
                                                if (result.getLayerErrors().size() > 0) {
                                                    for (Map.Entry<Layer, ArcGISRuntimeException> layerError : result.getLayerErrors().entrySet()) {
                                                        Log.e(TAG, "Error occurred when taking " + layerError.getKey().getName() + " offline.");
                                                        Log.e(TAG, "Error Message: " + layerError.getValue().getMessage());
                                                        Log.e(TAG, "Error Additional Message: " + layerError.getValue().getAdditionalMessage());
                                                        Log.e(TAG, "Error Cause: " + layerError.getValue().getCause());
                                                        Log.e(TAG, "Error Code: " + layerError.getValue().getErrorCode());
                                                        if (layerError.getValue().getErrorDomain() != null) {
                                                            Log.e(TAG, "Error Domain: " + layerError.getValue().getErrorDomain().name());
                                                        }
                                                    }
                                                    Toast.makeText(context, "Offline failed with layer errors", Toast.LENGTH_LONG).show();
                                                }
                                                if (result.getTableErrors().size() > 0) {
                                                    for (Map.Entry<FeatureTable, ArcGISRuntimeException> tableError : result.getTableErrors().entrySet()) {
                                                        Log.e(TAG, "Error occurred when taking " + tableError.getKey().getTableName() + " offline.");
                                                        Log.e(TAG, "Error : " + tableError.getValue().getMessage());
                                                    }
                                                    Toast.makeText(context, "Offline failed with table errors", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else {
                        // If loading failed, write to log and notify user
                        Log.e(TAG, "Map didn't load - status: " + map.getLoadStatus(), map.getLoadError().getCause());
                    }
                }
            });
            // Trigger map load - in the callback above we save the map for offline usage
            map.loadAsync();

            // Retry and timeout if needed
            map.addLoadStatusChangedListener(new LoadStatusChangedListener(){
                @Override
                public void loadStatusChanged(LoadStatusChangedEvent loadStatusChangedEvent) {
                    Log.i(TAG, "loadStatusChangedEvent: " + loadStatusChangedEvent.getNewLoadStatus().name());
                }
            });

            // Retry after 10 seconds
            new CountDownTimer(10000, 10000) {
                @Override
                public void onTick(long l) {
                    // Do nothing
                }

                @Override
                public void onFinish() {
                    // If didn't load - retry
                    if (map.getLoadError() != null || !LoadStatus.LOADED.equals(map.getLoadStatus())) {
                        Log.i(TAG, "Reloading because status was: " + map.getLoadStatus().name() + ". Error was: " + (map.getLoadError() != null ? map.getLoadError().toString() : " no error"));
                        map.retryLoadAsync();
                    }
                }
            }.start();

            // Timeout after 30 seconds if  not loaded yet - show a message to the user to retry
            new CountDownTimer(30000, 30000) {
                @Override
                public void onTick(long l) {
                    // Do nothing
                }

                @Override
                public void onFinish() {
                    // If didn't load - retry
                    if (map.getLoadError() != null || !LoadStatus.LOADED.equals(map.getLoadStatus())) {
                        Log.e(TAG, "Timeout because status was: " + map.getLoadStatus().name() + ". Error was: " + (map.getLoadError() != null ? map.getLoadError().toString() : " no error"));
                        map.cancelLoad();
                        Toast.makeText(context, "Map didn't load", Toast.LENGTH_LONG).show();
                    }
                }
            }.start();
        }
        catch (Exception e) {
            Log.e(TAG, "exception caught", e);
        }
    }

    /*
     * This method returns the ArcGIS polygon geometry (Web-Mercator SpatialReference) corresponding the OfflineBox for this route.
     */
    private Geometry getOfflineBoxGeometry(SpatialReference spatialReference) {
        double maxY = 37.936545613293596;
        double minX = -121.83111539108299;
        double minY = 37.8074458440129;
        double maxX = -121.99980980500101;

        PolygonBuilder offlineBoxCorners = new PolygonBuilder(SpatialReferences.getWgs84());
        offlineBoxCorners.addPoint(minX, maxY);
        offlineBoxCorners.addPoint(minX, minY);
        offlineBoxCorners.addPoint(maxX, minY);
        offlineBoxCorners.addPoint(maxX, maxY);

        Polygon wgs84Polygon = offlineBoxCorners.toGeometry();
        if (spatialReference != null && !SpatialReferences.getWgs84().equals(spatialReference)) {
            return GeometryEngine.project(wgs84Polygon, spatialReference);
        }
        else {
            return wgs84Polygon;
        }
    }

    private void setPercentage(final int percentage) {

    }
}
