package com.easy2hike.easyandroid.infra;

import com.esri.arcgisruntime.security.AuthenticationChallenge;
import com.esri.arcgisruntime.security.AuthenticationChallengeHandler;
import com.esri.arcgisruntime.security.AuthenticationChallengeResponse;
import com.esri.arcgisruntime.security.UserCredential;

/**
 * This simple implementation of {@link AuthenticationChallengeHandler} creates a {@link UserCredential}
 * and returns it in the {@link AuthenticationChallengeResponse}.
 *
 * Created by uri on 3/11/2018.
 */
public class ArcGISUserCredentialAuthenticationChallengeHandler implements AuthenticationChallengeHandler {

    private String token;

    ArcGISUserCredentialAuthenticationChallengeHandler(String token) {
        this.token = token;
    }

    @Override
    public AuthenticationChallengeResponse handleChallenge(AuthenticationChallenge challenge) {

        try {
            // The token was received by App Login
            UserCredential credential = UserCredential.createFromToken(token, "Easy2Hike");
            credential.setUsername("easy2hike");
            return new AuthenticationChallengeResponse(AuthenticationChallengeResponse.Action.CONTINUE_WITH_CREDENTIAL, credential);
        } catch (Exception e) {
            return new AuthenticationChallengeResponse(AuthenticationChallengeResponse.Action.CANCEL, null);
        }
    }
}
