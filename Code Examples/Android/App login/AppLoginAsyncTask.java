package com.easy2hike.easyandroid.infra;

import android.os.AsyncTask;
import android.util.Log;

import com.esri.arcgisruntime.security.AuthenticationChallengeHandler;
import com.esri.arcgisruntime.security.AuthenticationManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * This task sends a multipart/form-data request to ArcGIS online in order to receive a security token.
 * When the token is received it pushes it to the AuthenticationMananger via our custom {@link ArcGISUserCredentialAuthenticationChallengeHandler}.
 *
 * To activate it call:
 * new AppLoginAsyncTask().execute("https://www.arcgis.com/sharing/rest/oauth2/token/", clientId, clientSecret, "client_credentials");
 *
 * Created by uri on 07/31/2018.
 */
public class AppLoginAsyncTask extends AsyncTask<String, Void, Void> {

    private static final String ARCGIS_TOKEN_JSON_FIELD_NAME = "access_token";
    private static final String TAG = "AppLoginAsyncTask";

    public AppLoginAsyncTask() {
    }

    @Override
    protected Void doInBackground(String... params) {

        try {
            HTTPMultipartUtility multipart = new HTTPMultipartUtility(params[0]);
            multipart.addFormField("client_id", params[1]);
            multipart.addFormField("client_secret", params[2]);
            multipart.addFormField("grant_type", params[3]);
            // This parameter is not really needed for ArcGIS app-login - it was added because a bug in HTTPMultipartUtility was damaging the last param
            multipart.addFormField("f", "json");

            String response = multipart.finish();
            JSONObject responseJSON = new JSONObject(response);
            // Get the token out of the response json
            String token = responseJSON.getString(ARCGIS_TOKEN_JSON_FIELD_NAME);

            // We simply override the DefaultAuthenticationChallengeHandler with our own implementation.
            AuthenticationChallengeHandler authChallengeHandler = new ArcGISUserCredentialAuthenticationChallengeHandler(token);
            AuthenticationManager.setAuthenticationChallengeHandler(authChallengeHandler);
        } catch (IOException ex) {
            Log.i(TAG,"Error occurred while sending app login request: ", ex);
        } catch (JSONException ex) {
            Log.i(TAG,"Error occurred while parsing app login response: ", ex);
        }
        return null;
    }
}